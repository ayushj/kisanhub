from django.conf.urls import url

from .views import get_climate_all, get_climate, view_data, export_to_csv


urlpatterns = [url(r'^$', get_climate, name='home'),
               url(r'^api/getClimate/all$', get_climate_all, name='all'),
               url(r'^api/getClimate/(?P<country_name>[\w\-]+)$', get_climate_all, name='climate_by_country'),
               url(r'^api/viewData/$', view_data, name='view_all'),
               url(r'^api/viewData/export_to_csv/$', export_to_csv, name='export_to_csv'),
               ]
