from django.apps import AppConfig


class ClimateConfig(AppConfig):
    name = 'kisanhub.uk_climate'

    def ready(self):
        import kisanhub.uk_climate.signals