from django.db import models

from django.utils.encoding import python_2_unicode_compatible

from django_extensions.db.models import TitleSlugDescriptionModel
from django_extensions.db.models import TimeStampedModel


@python_2_unicode_compatible
class Climate(TimeStampedModel):
    """ Climate model to store all the information related to a climates of countries. """

    year = models.IntegerField()

    jan = models.FloatField(null=True, blank=True)
    feb = models.FloatField(null=True, blank=True)
    mar = models.FloatField(null=True, blank=True)
    apr = models.FloatField(null=True, blank=True)
    may = models.FloatField(null=True, blank=True)
    jun = models.FloatField(null=True, blank=True)
    jul = models.FloatField(null=True, blank=True)
    aug = models.FloatField(null=True, blank=True)
    sep = models.FloatField(null=True, blank=True)
    oct = models.FloatField(null=True, blank=True)
    nov = models.FloatField(null=True, blank=True)
    dec = models.FloatField(null=True, blank=True)

    win = models.FloatField(null=True, blank=True)
    spr = models.FloatField(null=True, blank=True)
    sum = models.FloatField(null=True, blank=True)
    aut = models.FloatField(null=True, blank=True)

    ann = models.FloatField(null=True, blank=True)

    prop = models.CharField(max_length=50)

    region = models.CharField(max_length=50)

    def __str__(self):
        return str(self.year)

    def save(self, *args, **kwargs):
        super(Climate, self).save(*args, **kwargs)

    @staticmethod
    def get_min_temp():
        result = Climate.objects.filter(prop='Tmin').exclude(ann=None).order_by('win')[0]
        return result

    @staticmethod
    def get_max_temp():
        result = Climate.objects.filter(prop='Tmax').latest('sum')
        return result

    @staticmethod
    def get_max_rainfall():
        result = Climate.objects.filter(prop='Rainfall').exclude(ann=None).order_by('-ann')[0]
        return result