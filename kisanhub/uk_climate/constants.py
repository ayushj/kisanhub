columns = ['Year', 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC', 'WIN', 'SPR',
           'SUM', 'AUT', 'ANN']

properties = ['Tmax', 'Tmin', 'Tmean', 'Sunshine', 'Rainfall']

countries = ['UK', 'England', 'Wales', 'Scotland']

base_url = 'http://www.metoffice.gov.uk/pub/data/weather/uk/climate/datasets/'

weather_file_dir = "kisanhub/uk_climate/climate_files/"