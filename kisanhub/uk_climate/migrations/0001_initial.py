# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-24 13:22
from __future__ import unicode_literals

from django.db import migrations, models
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Climate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(blank=True, null=True, verbose_name='description')),
                ('slug', django_extensions.db.fields.AutoSlugField(blank=True, editable=False, populate_from='title', verbose_name='slug')),
                ('year', models.IntegerField()),
                ('jan', models.FloatField(blank=True, null=True)),
                ('feb', models.FloatField(blank=True, null=True)),
                ('mar', models.FloatField(blank=True, null=True)),
                ('apr', models.FloatField(blank=True, null=True)),
                ('may', models.FloatField(blank=True, null=True)),
                ('jun', models.FloatField(blank=True, null=True)),
                ('jul', models.FloatField(blank=True, null=True)),
                ('aug', models.FloatField(blank=True, null=True)),
                ('sep', models.FloatField(blank=True, null=True)),
                ('oct', models.FloatField(blank=True, null=True)),
                ('nov', models.FloatField(blank=True, null=True)),
                ('dec', models.FloatField(blank=True, null=True)),
                ('win', models.FloatField(blank=True, null=True)),
                ('spr', models.FloatField(blank=True, null=True)),
                ('sum', models.FloatField(blank=True, null=True)),
                ('aut', models.FloatField(blank=True, null=True)),
                ('ann', models.FloatField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
