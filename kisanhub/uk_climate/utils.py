from itertools import groupby

from .models import Climate

from .constants import base_url, weather_file_dir, columns


def get_file_name(country, prop):
    return weather_file_dir+country+'_'+prop+'.txt'


def get_url(country, prop):
    return base_url+prop+'/date/'+country+'.txt'


def get_query_params(request, param):
    try:
        return request.GET.get(param)
    except KeyError:
        return None


def save_data(file_name, country, prop):
    max_space_string_val = '        '
    with open(file_name) as input_data:

        # Skips text before the beginning of the interesting columns:
        for line in input_data:
            if line.split() == columns:
                max_space = max([(len(list(cpart))) for c, cpart in groupby(line) if c == ' '])
                max_space_string_val = " " * (max_space+1)
                break

        # Reads text until the end of the block:
        for line in input_data:  # This keeps reading the file

            line = line.replace(max_space_string_val, '  --  ')

            val_list = line.split()

            climate_dict = {'prop': prop, 'region': country}

            for idx, val in enumerate(columns):
                try:
                    val_list[idx] = float(val_list[idx])
                except ValueError:
                    val_list[idx] = None
                except IndexError:
                    break
                climate_dict.update({val.lower(): val_list[idx]})

            if not Climate.objects.filter(year=int(climate_dict['year']),
                                          region=climate_dict['region'],
                                          prop=climate_dict['prop']).exists():
                Climate.objects.create(**climate_dict)
            # else:
            #     Update the existing object or do whatever required.
