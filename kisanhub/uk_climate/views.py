from django.template import loader
from django.http import HttpResponse
from django.shortcuts import redirect
from django.core.urlresolvers import reverse

import requests
import logging

from .models import Climate
from .utils import get_file_name, get_url, get_query_params, save_data
from .constants import countries, properties, weather_file_dir


def get_climate(request):
    template = loader.get_template("uk_climate/climate_home.html")
    return HttpResponse(
        template.render({"countries": countries, 'requested_countries': countries}, request))


def get_climate_all(request, country_name=None):
    requested_countries = countries

    if country_name:
        requested_countries = [country_name]

    for country in requested_countries:
        for prop in properties:

            file_name = get_file_name(country, prop)

            with open(file_name, 'wb') as input_data:

                url = get_url(country, prop)
                response = requests.get(url, stream=True)

                if response.ok:
                    content = response.content
                    input_data.write(content)

                    save_data(file_name, country, prop)
    if country_name is not None:
        return redirect(reverse('view_all') + '?country=' + country_name)
    else:
        return redirect(reverse('view_all'))


def view_data(request):
    try:
        country = get_query_params(request, 'country')

        if country is not None:
            climates = Climate.objects.filter(region__iexact=country)
        else:
            climates = Climate.objects.all()

        climate = Climate()
        max_temp = climate.get_max_temp()
        min_temp = climate.get_min_temp()
        max_rainfall = climate.get_max_rainfall()

    except Exception as e:
        logging.error(e.args)

    template = loader.get_template("uk_climate/chart.html")
    return HttpResponse(
        template.render({"climates": climates,
                         "countries": countries,
                         'max_temp': max_temp,
                         'min_temp': min_temp,
                         'max_rainfall': max_rainfall}, request))


from djqscsv import render_to_csv_response


def export_to_csv(request):
    country = get_query_params(request, 'country')
    qs = Climate.objects.filter(region__iexact=country).values('region', 'year', 'prop', 'jan', 'feb', 'mar', 'apr',
                                                               'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec',
                                                               'win', 'sum', 'aut', 'spr')
    return render_to_csv_response(qs)