from django.contrib import admin

from .models import Climate


@admin.register(Climate)
class ClimateAdmin(admin.ModelAdmin):
    list_filter = ["created", "modified"]
    list_display = ['__str__', 'id', 'region', 'prop', 'ann']
    search_fields = ['region', 'prop']
